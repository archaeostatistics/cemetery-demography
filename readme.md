# Plotting The Demography of A Cemetery
<img src="demography-01.png" ></img>

## Getting Started with RStudio.

### Install or update these packages.
``` r
install.packages(c("ggplot2", "ggalluvial", "ggthemes"))
```

### Load the following libraries.
``` r
library(ggplot2)
library(ggalluvial)
library(ggthemes)
```

### Read the sample data into the environment.
```
demography <- read.csv(file("demography.csv"))
```

### Inspect the loaded data.
``` r
View(demography)
```

## Creating an alluvial plot.

### Plot with theme_fivethirtyeight (demography-01.png).
``` r
demography_plot_01 <-
ggplot(data = demography,
       aes(axis1 = Beigaben, axis2 = Geschlecht, axis3 = Alter,
           y = Individuen)) +
  scale_x_discrete(limits = c("Beigaben", "Geschlecht", "Alter"), expand = c(.1, .05)) +
  xlab("Demografie") +
  geom_alluvium(aes(fill = Bestattungsart, color = Bestattungsart)) +
  geom_stratum() + geom_text(stat = "stratum", infer.label = TRUE, color = "black") +
  scale_fill_manual(values  = c("darkorange", "darkgreen")) +
  scale_color_manual(values = c("darkorange", "darkgreen")) +
  ggtitle("Verteilung von Beigaben und Bestattungsarten", 
          "Bronzezeitliches Gräberfeld von Falkenfurt") +
  theme_fivethirtyeight() +
  theme(
    axis.text.x = element_text(size = 12, face = "bold"),
    axis.title = element_text(), axis.title.x = element_blank()
  ) + ylab('Individuen')
```

### Plot with custom theme (demography-02.png).
``` r
library(extrafont) # Load additional fonts.

demography_plot_02 <-
ggplot(data = demography,
       aes(axis1 = Beigaben, axis2 = Geschlecht, axis3 = Alter,
           y = Individuen)) +
  scale_x_discrete(limits = c("Beigaben", "Geschlecht", "Alter"), expand = c(.1, .05)) +
  xlab("Demografie") +
  geom_alluvium(aes(fill = Bestattungsart, color = Bestattungsart)) +
  geom_stratum() + geom_text(stat = "stratum", infer.label = TRUE, color = "#343434", family = "Source Sans Pro") +
  scale_fill_manual(values  = c("darkorange", "darkgreen")) +
  scale_color_manual(values = c("darkorange", "darkgreen")) +
  ggtitle("Verteilung von Beigaben und Bestattungsarten", 
          "Bronzezeitliches Gräberfeld von Falkenfurt") +
  theme(
    text = element_text(color = "#343434", size = 16,  family = "Source Sans Pro"),
    title = element_text(color = "#343434", size = 12, family = "Source Sans Pro", face = "bold", ),
    axis.text.x = element_text(size = 12, family = "Source Sans Pro", face = "bold")
  )
```

### Export plots.
Use GUI or command-line interface.
``` r
png(file="demography-01.png", width = 1440, height = 825, units = "px")
demography_plot_01
dev.off()

png(file="demography-01.png", width = 1440, height = 825, units = "px")
demography_plot_02
dev.off()
```
